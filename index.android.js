/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  BackAndroid,
  ToastAndroid
} from 'react-native';

import {Scene, Reducer, Router, Switch, TabBar, Modal, Schema, Actions} from 'react-native-router-flux';

import Home from './src/components/home';
import App from './src/components/app';
import Test from './src/components/test';
import ListView from './src/components/listView';
import MovieScreen from './src/components/movieScreen';
import Signature from './src/components/signature';
import Login from './src/components/login';
import Register from './src/components/register';
import NavigationDrawer from './src/components/drawer';
import BarcodeScanner from './src/components/barcode';



const reducerCreate = params=>{
    const defaultReducer = Reducer(params);
    console.log("reducerParams",params)
    return (state, action)=>{

        console.log('state',state);
        console.log('action',action);
        
        if(action.type!='back')
        {
          console.log("ACTION:", action);
          return defaultReducer(state, action);
        }
        return state;
    }
};



class TabIcon extends React.Component {
    render(){
        return (
            <Text style={{color: this.props.selected ? "red" :"black"}}>{this.props.title}</Text>
        );
    }
}

class Right extends React.Component {
    render(){
        return <Text style={{
        width: 80,
        height: 37,
        position: "absolute",
        bottom: 4,
        right: 2,
        padding: 8,
        color:"white"
    }}>Right</Text>
    }
}

import errorView from './src/components/errorView';
import loadingView from './src/components/loadingView';
import notificationView from './src/components/notificationView';

const scenes = Actions.create(
			<Scene style={{backgroundColor:"cornflowerblue"}} key="modal" component={Modal} >
				<Scene initial={true} style={{backgroundColor:"cornflowerblue"}} key="root" hideNavBar={true} >
            <Scene style={{backgroundColor:"cornflowerblue"}}  key="app" hideNavBar={true} component={App} title="App"/>
            <Scene duration={0} key="loginModal"  >
                <Scene key="loginMain" tabs={true} default="login"  direction="vertical" >
                  <Scene duration={0} style={{backgroundColor:"cornflowerblue"}} key="login" hideNavBar={true} component={Login} title="Login"/>
                  <Scene duration={0} style={{backgroundColor:"cornflowerblue"}} key="register" hideNavBar={true} component={Register} title="Register"/>
                </Scene>
            </Scene>
      			<Scene duration={0} key="tabbar" type="reset" component={NavigationDrawer}>
						  <Scene key="main" duration={0} tabs={true} default="tab2" >
							   <Scene duration={0}  key="tab2" initial={true} navigationBarStyle={{backgroundColor:"cornflowerblue"}} titleStyle={{color:"white"}}  title="home"  component={Home}   />
							   <Scene duration={0}  key="tab1" navigationBarStyle={{backgroundColor:"cornflowerblue"}} titleStyle={{color:"white"}}  title="Test"  component={Test}   />
        				 <Scene key="tab4"  navigationBarStyle={{backgroundColor:"cornflowerblue"}} titleStyle={{color:"white"}}  title="ListTab" >                
            				<Scene duration={100} direction="horizontal" key="tab4_1" navigationBarStyle={{backgroundColor:"cornflowerblue"}} titleStyle={{color:"white"}}  title="ListView2" onRight={()=>{alert(this);console.log(this)}} rightTitle="Right" component={ListView}   />
            				<Scene duration={100} direction="horizontal" key="tab4_2" navigationBarStyle={{backgroundColor:"cornflowerblue"}} titleStyle={{color:"white"}}  title="MovieScreen"  component={MovieScreen}   />
          			 </Scene>
							   <Scene key="tab3" component={BarcodeScanner} title="BarcodeScanner"/>
						  </Scene>
				   </Scene>
					<Scene key="tab4_3" schema="modal"  title="Signature"  component={Signature}  />
				</Scene>
        <Scene key="error" component={errorView} />
        <Scene key="loadingIndicator" component={loadingView} />
        <Scene key="notification" component={notificationView} />
			</Scene>
);

import {Provider} from 'react-redux';
import store from './src/store/storeMain.js';

class parcel extends Component {

  componentWillMount(){
    BackAndroid.addEventListener('hardwareBackPress', () => {
        try {
            Actions.pop();
            return true;
        }
        catch (err) {
            ToastAndroid.show("Cannot pop. Exiting the app...", ToastAndroid.SHORT);
            return false;
        }
    });


  }
  
  render() {
    return (
      <Provider store={store()}>
    		<Router style={{backgroundColor:"cornflowerblue"}} sceneStyle={{backgroundColor:'cornflowerblue'}} createReducer={reducerCreate} scenes={scenes} >
    		</Router>
     </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('parcel', () => parcel);
