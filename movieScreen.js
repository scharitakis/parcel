 import React,
{
  Component,
  StyleSheet,
  Text,
  View
}
from 'react-native';

class Home extends Component{
	constructor(props) {
		super(props);
	}

	render() {
		return (
		<View style={styles.container}>
			<Text style={styles.instructions}>
				This is Home,{'\n'}
				Cmd+D or shake for dev menu
			</Text>
		</View>	
		);
	}
	
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default Home;