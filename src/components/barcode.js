import React, {
 	Component,
	StyleSheet,
	View,
	Text,
} from 'react-native';
import BarcodeScanner from 'react-native-barcodescanner';

class BarcodeScannerComp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      torchMode: 'off',
      cameraType: 'back',
	  barcodeData:"",
	  hideCam:true
    };
  }

  barcodeReceived(e) {
    console.log('Barcode: ' + e.data);
    console.log('Type: ' + e.type);
	this.setState({barcodeData:e.data})
  }
  
  stopme(){
	  var stateHideCam=this.state.hideCam;
	  this.setState({hideCam:!stateHideCam})
  }
  
  renderCam(){
	  return(<BarcodeScanner
					onBarCodeRead={this.barcodeReceived.bind(this)}
					style={{ flex: 1 }}
					torchMode={this.state.torchMode}
					cameraType={this.state.cameraType}
					viewFinderDrawLaser={true}
				/>)
  }
  
  renderBtn(){
	  return(<Text>noCam </Text>)
  }

  render() {
	 
    var barcodeData = this.state.barcodeData;	
	var camCont = this.state.hideCam?this.renderBtn():this.renderCam();
		
    return (
		<View>
			<View style={styles.test}>
				{camCont}
			</View>
			<View>
			<Text>{barcodeData}</Text>
			<Text onPress={this.stopme.bind(this)}>Stop</Text>
			</View>
		</View>
	
    );
  }
}


const styles = StyleSheet.create({
	test:{
		height:300
	},
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#F5FCFF",
    },
    welcome: {
        fontSize: 20,
        textAlign: "center",
        margin: 10,
    },
    instructions: {
        textAlign: "center",
        color: "#333333",
        marginBottom: 5,
    },
});

export default BarcodeScannerComp;