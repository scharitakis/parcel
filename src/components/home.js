 import React,
{
  Component,
  StyleSheet,
  Text,
  View
}
from 'react-native';

import {connect} from 'react-redux';
import Button from "react-native-button";



const CounterView = ({value,onIncrement,onDecrement})=>(
<View style={styles.container}>
	<Text style={styles.instructions}>
		This is Home Counter,{'\n'}
		Cmd+D or shake for dev menu
	</Text>
	<Text style={styles.instructions}>
		{value}
	</Text>
	<Button onPress={onIncrement}> Increment </Button>
	<Button onPress={onDecrement}> Decrement </Button>
</View>	
)



class Home extends Component{
	render() {
		return (
		 <CounterView value={this.props.state.counter}
		 onIncrement={()=>this.props.dispatch({type:'INCREMENT'})}
		 onDecrement={()=>this.props.dispatch({type:'DECREMENT'})}
		 />
		);
	}
	
}

Home = connect((state)=>({state}))(Home)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default Home;