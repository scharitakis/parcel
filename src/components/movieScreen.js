 import React,
{
  Component,
  StyleSheet,
  Text,
  View,
  Image
}
from 'react-native';

import {Actions} from "react-native-router-flux";

var  MovieScreen = React.createClass({

	componentDidMount:function() {
    	console.log("componentdidMount MovieScreen")
  	},

  	gotoSignature:function(){
  			Actions.tab4_3({title:'Signature' });
  	},
	
	renderImage:function(){
		
		return(<Image style={{width:60,height:60,backgroundColor:"green"}} source={{uri:base64}}/>)
	},

	render:function() {
		var data = this.props.data
		var base64 = (data.base64 )?'data:image/png;base64,'+data.base64 :'';
		console.log('mydata================',data);
		return (
		<View style={styles.container}>
			<Text style={styles.instructions}>
				this is the {data.title},{'\n'}
				Cmd+D or shake for dev menu
			</Text>
			<Text onPress={this.gotoSignature} style={styles.instructions}>
				Pressme
			</Text>
			
		</View>	
		);
	}
	
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default MovieScreen;