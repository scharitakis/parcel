import React, {
    View,
    Text,
    StyleSheet,
    TextInput
} from "react-native";

import {Actions} from "react-native-router-flux";
import {connect} from 'react-redux';

import Button from "react-native-button";


/*import {
  register
} from '../actions/actionsMain';*/

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "cornflowerblue",
        margin:10
    },
    welcome: {
        fontSize: 20,
        textAlign: "center",
        margin: 10,
    },
    instructions: {
        textAlign: "center",
        color: "#333333",
        marginBottom: 5,
    },
});


 class Register extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username:"",
            password:""
        }
    }

    validate(){
         if(this.state.username !="" && this.state.password!=""){
            this.props.dispatch(login(this.state));
         }
    }

    render(){
        var username ="false"
        if(this.props.state.login.username){
            username = "true"
        }
        return (
            <View style={[styles.container, this.props.style]}>

                <View style={{height:100,justifyContent: "center",alignItems: "center",}}>
                    <Text style={{color:"white"}}>Register: {username}</Text>
                </View>
                <View>
                    <Text style={{color:"white"}}>Username:</Text>
                    <TextInput
                        style={{height: 40}}
                        onChangeText={(username) => this.setState({username})}
                        value={this.state.username}                    
                     />
                     <Text style={{color:"white"}}>Password:</Text>
                      <TextInput
                        style={{height: 40}}
                        onChangeText={(password) => this.setState({password})}
                        value={this.state.password}     
                     />
                    <Button style={{color:"white"}} onPress={() => {
                            //this.validate();                       

                    }}>Register</Button>
                    <Button style={{color:"white"}} onPress={Actions.login}>Login</Button>
                </View>
            </View>
        );
    }
}

Register = connect((state)=>({state}))(Register)  

export default Register