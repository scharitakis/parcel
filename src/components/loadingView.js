import React, {View, Text, StyleSheet, Animated, Dimensions} from "react-native";
import Button from "react-native-button";
import {Actions} from "react-native-router-flux";
var ProgressBar = require('ProgressBarAndroid');

var styles = StyleSheet.create({
    container: {
        position: "absolute",
        top:0,
        bottom:0,
        left:0,
        right:0,
        backgroundColor:"rgba(52,52,52,0.5)",
        justifyContent: "center",
        alignItems: "center",
    },
});

class LoadingIndicator extends React.Component {
	componentWillUnmount(){
		console.log("LoadingIndicator Unmounted")
	}

    render(){
        return (
            <View style={styles.container}>
                <View style={{  width:150,
                                height:150,
                                justifyContent: "center",
                                alignItems: "center",
                                backgroundColor:"transparent" }}>
                    <ProgressBar styleAttr="Inverse" />
                    <Button onPress={Actions.pop}>Close</Button>
                </View>
            </View>
        );
    }
}


export default LoadingIndicator;