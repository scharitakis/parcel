 import React,
{
  Component,
  StyleSheet,
  Text,
  View
}
from 'react-native';

import SignatureCapture from 'react-native-signature-capture';

import {Actions} from "react-native-router-flux";

class Signature extends Component{
    constructor(props) {
      super(props);

      this.state = {
       	hideSignature:true
      };
    }
	
	_onSaveEvent(result) {
	    //result.encoded - for the base64 encoded png
	    //result.pathName - for the file path name
	    console.log(result);
      	console.log(result.encoded)
		  Actions.pop();
	}
	
    stopme(){
  	  var hideSignature=this.state.hideSignature;
  	  this.setState({hideSignature:!hideSignature})
    }
	
	renderSignature(){
		return ( <SignatureCapture 
			rotateClockwise={true}
			square={true}
			onSaveEvent={this._onSaveEvent}/>)
	}
	
	returnT(){
		return (<Text>test</Text>)
	}

	render() {
		var retComp = this.state.hideSignature?this.returnT():this.renderSignature();
		return (
		<View style={styles.test}>
			<View style={styles.test}>
				{retComp}
			</View>
			<Text onPress={this.stopme.bind(this)}>Stop</Text>
		</View>
		);
	}
	
}

const styles = StyleSheet.create({
  test:{
	  height:200,
    marginTop:60
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',

  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default Signature;