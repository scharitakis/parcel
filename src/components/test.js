 import React,
{
  Component,
  StyleSheet,
  Text,
  TextInput,
  View,
	ListView
}
from 'react-native';

import Button from "react-native-button";
import {connect} from 'react-redux';

import {
	AddTodoAction,
	toggleTodoAction,
	removeTodoAction,
	removeAllTodoAction,
	makeASandwichWithSecretSauce
} from '../actions/actionsMain';

//import store from '../store/storeMain.js';



class MyListView extends Component{
	constructor(props) {
	    super(props);
		this.state = {
        	dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
		}
	} 
	/*componentDidMount(){
		let store = this.context.store;
		this.unsubscribe = store.subscribe(()=>this.forceUpdate())
	}*/
	
	renderRowTodo(data){
		return(
			<View><Text>{data.text}</Text><Text>{data.completed?'true':'false'}</Text></View>
		)
	}
	
	render(){
		let state = this.props.state;
		let todosDataSource = this.state.dataSource.cloneWithRows(state.todos)
		return(
	      <ListView
			enableEmptySections={true}
	        dataSource={todosDataSource}
	        renderRow={this.renderRowTodo}
	        style={{flex:1}}
	      ></ListView>
		)
	}
}

MyListView = connect((state)=>({state}))(MyListView)	



class TodosView extends Component{
	
	constructor(props) {
	    super(props);
		this.state = {
			text:""
		}
	}

	componentDidMount(){
		console.log('TodosView componentDidMount')
	}

	componentWillUnmount(){
		console.log('TodosView componentWillUnmount')
	}
	
	render(){
		console.log('mystate=',this.props)
		return(
			<View style={styles.container}>
	      		<MyListView />
				<TextInput 
				    ref={component=>this._textInput=component}
    				style={{height: 40, borderColor: 'gray', borderWidth: 1}}
	    			onChangeText={(text) => this.setState({text})}
					value={this.state.text}
				></TextInput>
				<Button
					onPress={()=>{
						//console.log(this._textInput.text)
						this.props.dispatch(AddTodoAction(this.state.text));
						this.setState({text:""})
						//this._textInput.setNativeProps({text: ''});
						}
					}
					onSubmitEditing={
						()=>{console.log("enter was pressed")}
					}
					onEndEditing={()=>{console.log("end edditing")}}
				>ADDTODO</Button>
				<Button
					onPress={()=>this.props.dispatch(toggleTodoAction(0))}
				>TOGGLETODO</Button>
				<Button
					onPress={()=>this.props.dispatch(removeTodoAction(0))}
				>remove</Button>
				<Button
					onPress={()=>this.props.dispatch(removeAllTodoAction())}
				>removeALL</Button>
				<Button
					onPress={()=>this.props.dispatch(makeASandwichWithSecretSauce(0))}
				>testFetch</Button>
			</View>	
		)
	}
}

TodosView = connect((state)=>({state}))(TodosView)	



const Test = () => {
		return (
			<TodosView />
		);
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    backgroundColor: 'green',
    paddingTop:70,
    flex:1
  },
  text:{
    flex:1,
    justifyContent: 'center'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default Test;