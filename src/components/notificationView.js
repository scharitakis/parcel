import React, {View, Text, StyleSheet, Animated, Dimensions} from "react-native";
import Button from "react-native-button";
import {Actions} from "react-native-router-flux";
import {connect} from 'react-redux';
var ProgressBar = require('ProgressBarAndroid');

var {
  height: deviceHeight
} = Dimensions.get("window");

var styles = StyleSheet.create({
    container: {
    	position:"absolute",
    	top:0,
    	left:0,
    	right:0,
        backgroundColor:"red",
        justifyContent: "center",
        alignItems: "center",
    },
});




class Notification extends React.Component {
	componentWillUnmount(){
		console.log("Notification Unmounted")
	}

	componentDidMount(){
		//setTimeout(()=>{Actions.pop()},10000)
	}

    render(){
    	var notificationCenter = this.props.state.notificationCenter;
    	var showNotification = notificationCenter.msg?true:false
		if(showNotification){    	
	        return (
	            <View style={styles.container}>
	               <Text>{notificationCenter.msg}</Text>
	            </View>
	        );
	    }else{
	    	return (
	            <View style={styles.container}></View>
	        );
	    }
    }
}

Notification = connect((state)=>({state}))(Notification)  

export default Notification;