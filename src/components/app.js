import React, {View, Text, StyleSheet} from "react-native";
import {Actions} from "react-native-router-flux";
import {connect} from 'react-redux';


var ProgressBar = require('ProgressBarAndroid');

import Button from "react-native-button";


import {
  getLogin,
} from '../actions/actionsMain';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#F5FCFF",
    },
    welcome: {
        fontSize: 20,
        textAlign: "center",
        margin: 10,
    },
    instructions: {
        textAlign: "center",
        color: "#333333",
        marginBottom: 5,
    },
});


 class App extends React.Component {
    componentDidMount(){
    	//setTimeout(()=>{this.props.dispatch(getLogin())},3000)
        this.props.dispatch(getLogin())
        Actions.notification()
    }

    componentWillUnmount(){
    	console.log('App componentWillUnmount')
    }

    render(){
    	return (
            <View style={[styles.container, this.props.style]}>
                <Text>Initial page</Text>
                <Button style={{color:"white"}} onPress={Actions.error}>test</Button>
            </View>
        );
    }
}

App = connect((state)=>({state}))(App)  

export default App