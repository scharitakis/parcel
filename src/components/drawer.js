import React, {Component} from "react-native"
import NavigationMenu from "./navigationMenu"
import Drawer from "react-native-drawer"
import {DefaultRenderer} from "react-native-router-flux";

export default class extends Component {
    render(){
        const children = this.props.navigationState.children;
        return (
            <Drawer
                ref="navigation"
                type="overlay"
                content={<NavigationMenu />}
                tapToClose={true}
                openDrawerOffset={0.2}
                panCloseMask={0.2}
				closedDrawerOffset={-3}
				acceptPan={false}
				styles={{
					drawer: {shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3},
					main: {paddingLeft: 3}
				}}
				tweenDuration={50}
                tweenHandler={(ratio) => ({
					main: { opacity:(2-ratio)/2 }
					})}
				>
                <DefaultRenderer navigationState={children[0]} />
            </Drawer>
        );
    }
}