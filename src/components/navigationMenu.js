 import React,
{
  Component,
  StyleSheet,
  Text,
  View
}
from 'react-native';

import {connect} from 'react-redux';
import {Actions} from "react-native-router-flux";
import Button from "react-native-button";


import {
  logout
} from '../actions/actionsMain';





class NavigationMenu extends Component{
	render(){
		const drawer = this.context.drawer;
    var username = this.props.state.login.username;
		return(
			<View style={[styles.container, this.props.sceneStyle]}>
         <View style={{backgroundColor:"cornflowerblue",height:100,justifyContent:"center",borderBottomWidth:1,alignItems:'center',borderBottomColor:'dimgray'}}>
              <Text style={{color:"white"}}>Welcome:{username}</Text>
         </View>
         <View style={{flex:1}}>
    			   <View>
                <Button onPress={()=>{drawer.close();Actions.tab2();}} style={styles.menuBtns}>
    			       GoTo home
    			       </Button> 
             </View>
             <View >
                 <Button onPress={()=>{drawer.close();Actions.tab1();}} style={styles.menuBtns}>
        			    GoTo test
        			   </Button> 
             </View>
             <View >
                  <Button onPress={()=>{drawer.close();Actions.tab3();}} style={styles.menuBtns}>
                  GoTo barcodeScanner
                 </Button>
             </View>
             <View >
        			   <Button onPressIn={()=>{drawer.close();Actions.tab4();}} style={styles.menuBtns}>
                  GoTo ListView
                 </Button>		
             </View>            
         </View>
          <View style={{backgroundColor:"lightsteelblue"}}>   
                  <Button style={{color:"white",fontSize:14,margin:5}} onPress={()=>{drawer.close();this.props.dispatch(logout())}} >
                  Logout
                 </Button>
           </View>
      </View>
		)
	}
	
}
NavigationMenu.contextTypes = {
    drawer: React.PropTypes.object
};

NavigationMenu = connect((state)=>({state}))(NavigationMenu)  




const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: "white",
    },
    welcome: {
        fontSize: 20,
        textAlign: "center",
        margin: 10,
    },
    menuBtns: {
        textAlign: "left",
        color: "dimgray",
        fontSize: 12, 
        fontWeight:"normal",
        margin:20
    },
});

export default NavigationMenu;