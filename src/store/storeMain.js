import {createStore,applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import todoAppCompositionReducer from '../reducers/reducerMain'


const store = ()=>{
	return createStore(
  		todoAppCompositionReducer,
  		applyMiddleware(thunk)
	);
}

export default store