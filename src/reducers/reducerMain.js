import {combineReducers} from 'redux';




import Immutable from 'immutable'

const todoReducer = (state,action)=>{
	switch(action.type){
		case 'ADD_TODO':
			return {
			    	id:action.id,
					text:action.text,
					completed:false,
					testArr:[{t:true}]
			    };
		case 'REMOVE_TODO':
			return state.id !== action.id;
		case 'TOGGLE_TODO':
			if(state.id !==action.id){
				return state;
			}
				
			return {
					...state,
					completed:!state.completed
			};				
		default:
			return state;
	}
}


const todosReducer = (state=[],action)=>{
	state = Immutable.fromJS(state);
	switch(action.type){
		case 'ADD_TODO':
		    var n = Immutable.Map({
			    	id:action.id,
					text:action.text,
					completed:false,
					testArr:[{t:true}]
			 });
			return state.push(n).toJS();
		case 'REMOVE_ALL_TODO':
			return [];
		case 'REMOVE_TODO':
			return state.filter(todo=>{
				return todo.get('id')!==action.id	
			}).toJS();
		case 'TOGGLE_TODO':
			return state.map(todo=>{
				if(todo.get('id')!==action.id){
					return todo;
				}

				return todo.set('completed',!todo.get('completed'));
			}).toJS()
		case 'TOGGLE_ALL':
	  	  	return state.map(todo =>{
	  	  		return todo.set('completed',true)
	  	  	}).toJS();
		default:
			return state.toJS();
	}

	/*switch(action.type){
		case 'ADD_TODO':
			return [
				...state,
			    todoReducer(undefined,action)
				];
		case 'REMOVE_ALL_TODO':
			return [];
		case 'REMOVE_TODO':
			return state.filter(todo=>todoReducer(todo,action))
		case 'TOGGLE_TODO':
			return state.map(todo=>todoReducer(todo,action))
		case 'TOGGLE_ALL':
	  	  	const areAllMarked = state.every(todo => todo.completed)
	        return state.map(todo => Object.assign({}, todo, {
	          completed: !areAllMarked
	        }))
		default:
			return state;
	}*/
	
}


const visibilityFilterReducer= (state="SHOW_ALL",action)=>{
	switch(action.type){
		case 'SET_VISIBILITY_FILTER':
			return action.filter;
		default:
			return state;
	}
}



//------Counter Reducer

const CounterReducer = (state=0,action)=>{
	switch(action.type){
	case 'INCREMENT':
		return state + 1;
	case 'DECREMENT':
		return state - 1;
	default:
		return state;	
	
	}
}

//------Login Reducer

var store = require('react-native-simple-store');

var loginState = function(){
	//store.get('login')
	
	console.log("asdsada")
	return {}
}

const loginReducer = (state={},action)=>{
	//state = Immutable.fromJS(state);
	console.log('action loginReducer',action)
	switch(action.type){
	case 'SAVE_LOGIN':
		return {
				username:action.username,
				password:action.password,
				token:action.token
				};
	case 'LOGOUT':
		return {};
	default:
		return state;	
	
	}
}


const notificationCenterReducer = (state={},action)=>{
	switch(action.type){
	case 'CREATE_NOTIFICATION':
		return {
				msg:action.msg,
				ntype:"top"
				};
	case 'CLEAR_NOTIFICATION':
		return {};
	default:
		return state;	
	
	}
}




//------Composition Reducers

const todoAppCompositionReducer = combineReducers(
	{
		todos:todosReducer,
		visibilityFilter:visibilityFilterReducer,
		counter:CounterReducer,
		login:loginReducer,
		notificationCenter:notificationCenterReducer
		
	}
);

export default todoAppCompositionReducer;