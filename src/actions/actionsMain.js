
import {Actions} from "react-native-router-flux";


let todoId = 0;
const AddTodoAction = (text)=>{
	return {
		type:'ADD_TODO',
		text,
		id:todoId++,
	}
}

const toggleTodoAction = (id)=>{
	return {
		type:'TOGGLE_TODO',
		id:id,
	}
}

const removeTodoAction = (id)=>{
	return {
		type:'REMOVE_TODO',
		id:id,
	}
}

const removeAllTodoAction  = ()=>{
	todoId = 0;
	return {
		type:'REMOVE_ALL_TODO',
	}
}

const fetchSecretSauce =() =>{
	return fetch('https://www.google.com/search?q=secret+sauce');
}

const makeASandwichWithSecretSauce = (forPerson)=> {

  // Invert control!
  // Return a function that accepts `dispatch` so we can dispatch later.
  // Thunk middleware knows how to turn thunk async actions into actions.

  return function (dispatch) {
    return fetchSecretSauce()
	  	.then(response => response.text())
	    .then(text=>{
			console.log(text);
			dispatch(AddTodoAction('sauce'));
		})
		.catch((error) => {
		  console.log(error);
		});
  };
}	


var store = require('react-native-simple-store');


const LoginAction = (loginParams)=>{
	console.log('login',login)
	return {
		type:'SAVE_LOGIN',
		username:loginParams.username,
		password:loginParams.password,
		token:loginParams.token
	}
}




const loginFetch = (params)=>{
	return fetch('http://192.168.56.1:3000/',{
	  method: 'POST',
	  headers: {
	    'Accept': 'application/json',
	    'Content-Type': 'application/json',
	  },
	  body: JSON.stringify(params)
	})
	.then(response => response.json())
}



const login = (loginParams)=>{
	return function(dispatch,getState){
  		console.log(getState())
		loginFetch(loginParams)
		.then(resJSON=>{
				if(resJSON.token){
					loginParams.token = resJSON.token
					store.save("login",loginParams)
					.then(() => store.get('login'))
					.then(loginstore=>{
						dispatch(LoginAction(loginParams))
						Actions.tabbar()
					})
				}
			})
		.catch(error=>{
			console.log("error",error)
		})

	}
}

const getLogin = (testparam)=>{
	return function(dispatch){
		store.get("login")
		.then(loginParams=>{
			console.log('getLogin',loginParams)
			if(loginParams){
				dispatch(LoginAction(loginParams))
				Actions.tabbar()
			}else{
				Actions.loginModal()
			}
		})
		.catch(error=>{
			console.log("error",error)
		})
	}
}

const LogoutAction = ()=>{
	return {
		type:'LOGOUT'
	}
}

const logout = (testparam)=>{
	return function(dispatch){
		store.delete("login")
		.then(()=>{
				dispatch(LogoutAction())
				Actions.loginModal()
		})
		.catch(error=>{
			console.log("error",error)
		})
	}
}


/*NOTIFICATION CENTER*/

const createNotificationAction = (msg)=>{
	return {
		type:'CREATE_NOTIFICATION',
		msg:msg
	}
}


const clearNotificationAction = ()=>{
	return {
		type:'CLEAR_NOTIFICATION'
	}
}




/*NOTIFICATION CENTER END*/


export {
	createNotificationAction,
	clearNotificationAction,
	LoginAction,
	getLogin,
	login,
	logout,
	AddTodoAction,
	toggleTodoAction,
	removeTodoAction,
	removeAllTodoAction,
	makeASandwichWithSecretSauce
};